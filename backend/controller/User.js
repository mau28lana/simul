import Users from "../models/UserModel.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import isset from "isset";
import empty from "is-empty";

export const getUsers =  async(req, res) => {
    try{
        const users = await Users.findAll({
            attributes: ['id', 'username', 'email']
        });
        res.json(users);
    }catch(error){
        console.log(error);
    }
}

export const Register = async(req, res) => {
    const {username, email, password, confPassword} = req.body;
    const usernameExist = await Users.findOne({username: req.body.username});
    const emailExist = await Users.findOne({email: req.body.email});

    if(password !== confPassword) return res.status(400).json({msg: "Password and Confirm Password does not match!"});

    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    

    try{
        if(isset(username) && !empty(username)
        && isset(email) && !empty(email)
        && isset(password) && !empty(password)
        && isset(confPassword) && !empty(confPassword)
        ){ 
            await Users.findOne({username: req.body.username}).then(resp => {
                if(resp.length != 0 ){
                    return res.json({
                        data: [1],
                        success: false,
                        msg: "Email exist!"
                    })
                }else {
                    Users.create({
                        username: username,
                        email: email,
                        password: password,
                        confPassword: confPassword
                    });
                    res.json({msg: "account created!"})
                }
            })
        }else{
            res.json({msg:"Field cannot be empty!"});
        }
    }catch(e){
        console.log(e);
    }
}

export const Login = async(req, res) => {
    try {
        const users = await Users.findAll({
            where:{
                email: req.body.email
            }
        });
        const match = await bcrypt.compare(req.body.password, users[0].password);
        if(!match) return res.status(400).json({msg: "Wrong password!"});
        const userId = users[0].id;
        const username = users[0].username;
        const email = users[0].email;

        const accessToken = jwt.sign({userId, username, email}, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: "1m"
        });
        const refreshToken = jwt.sign({userId, username, email}, process.env.REFRESH_TOKEN_SECRET, {
            expiresIn: "1m"
        });

        await Users.update({refresh_token: refreshToken},{
            where:{
                id: userId
            }
        });

        res.cookie('refreshToken', refreshToken,{
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000,
        });

        res.json({accessToken});
    } catch (error) {
        res.status(404).json({msg: "Email does not found!"})
    }
}